import cv2
import dlib


mousePoints = []

def mouseEventHandler(event, x, y, flags, param):

    global mousePoints
    if event == cv2.EVENT_LBUTTONDOWN:     # starting coordinates
        mousePoints = [(x, y)]
    elif event == cv2.EVENT_LBUTTONUP:     # ending coordinates
        mousePoints.append((x, y))

    return




# Create a named window in OpenCv and attach the mouse event handler to it.
# The startWindowThread() is required when running this script through an
# interactive IPython session.

cv2.startWindowThread()
cv2.namedWindow("Video stream")
cv2.setMouseCallback("Video stream", mouseEventHandler)

# Print the instructions

print("Use the mouse to draw a rectangle around the object to be tracked")
print("Press 'CTRL-C' to stop the program")


# Create the video stream

video = cv2.VideoCapture(0)

# Initialize the correlation tracker.

tracker = dlib.correlation_tracker()

# The video processing loop

tracked = False

try:
    while True:
        
        readFlag, frame = video.read()
        if readFlag:
            image = frame
        else:
            print("Error: Cannot read frames from video stream")
            exit(1)

        # If we have two sets of coordinates from the mouse event, draw a rectangle.
        
        if len(mousePoints) == 2:
            cv2.rectangle(image, mousePoints[0], mousePoints[1], (0, 255, 0), 2)
            rectangle = dlib.rectangle(mousePoints[0][0], mousePoints[0][1], mousePoints[1][0], mousePoints[1][1])
            tracker.start_track(image, rectangle)
            tracked = True
            mousePoints = []
            
        # When not tracking, simply show the image. When tracking,
        # show the image + a rectange around the tracked object.
       
        if tracked == False:
            cv2.imshow("Video stream", image)
            cv2.waitKey(1)                       # Allows to show the image
        else:
            tracker.update(image)
            rectangle = tracker.get_position()
            x  = int(rectangle.left())
            y  = int(rectangle.top())
            x1 = int(rectangle.right())
            y1 = int(rectangle.bottom())
            cv2.rectangle(image, (x, y), (x1, y1), (0, 0, 255), 2)
            cv2.imshow("Video stream", image)
            cv2.waitKey(1)

# Allow the user to interrupt with Ctrl-c

except: 
    pass

# Clean up

video.release()
cv2.destroyAllWindows()

