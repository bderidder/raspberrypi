# Raspberry Pi 3 B+ Scripts

## Installation and configuration

* [Installation and configuration of Raspbian](InstallationNotes/Raspbian.md)
* [Installation of OpenCV](InstallationNotes/OpenCV.md)
* [Installation of dlib](InstallationNotes/dlib.md)
* [Installation of IPython](InstallationNotes/IPython.md)

## Interesting links

* [picamera](https://picamera.readthedocs.io/en/release-1.13/index.html)
* [OpenCV Python tutorials](https://docs.opencv.org/4.0.0/d6/d00/tutorial_py_root.html)
* [Dlib documentation](http://www.dlib.net/intro.html)

