
import cv2
import dlib
import time

# For the frontal face detection, use a pre-trained cascade classifier that uses Haar features.

classifier = cv2.CascadeClassifier('/home/pi/opencv-4.0.0/data/haarcascades/haarcascade_frontalface_default.xml')

cv2.startWindowThread()

# Create two windows and position them next to each other

cv2.namedWindow("base-image", cv2.WINDOW_AUTOSIZE)
cv2.namedWindow("result-image", cv2.WINDOW_AUTOSIZE)

cv2.moveWindow("base-image",0,100)
cv2.moveWindow("result-image",400,100)

video = cv2.VideoCapture(0)

# Keep on processing images from the video stream until the user interrupts.

frameCounter = 0
currentFaceID = 0

faceTrackers = {}
faceNames = {}

print("Use CTRL-C to exit the loop")

try:
    while True:

        # Read one image from the video and resize 

        readFlag, fullSizeImage = video.read()
        baseImage = cv2.resize(fullSizeImage, (320, 240))

        # The result image is the base image + rectangles over the faces
        
        resultImage = baseImage.copy()

        # Update all the trackers and remove the poor quality ones
        
        faceIDsToDelete = []
        for faceID in faceTrackers.keys():
            trackingQuality = faceTrackers[faceID].update(baseImage)
            if trackingQuality < 7:
                faceIDsToDelete.append(faceID)

        for faceID in faceIDsToDelete:
            faceTrackers.pop(faceID, None)


        # Every 10 frames, determine which faces are present in the frame

        if (frameCounter % 10) == 0:

            # Convert the baseImage to a gray image

            grayImage = cv2.cvtColor(baseImage, cv2.COLOR_BGR2GRAY)
            
            # Detect all faces
            
            faces = classifier.detectMultiScale(grayImage, 1.3, 5)

            # Check for each face if we're already tracking it, and if not start a tracker. 

            for (_x,_y,_w,_h) in faces:
            
                # Convert from numpy.int32 to a regular Python int

                x = int(_x)        # x-coordinate of bottom left
                y = int(_y)        # y-coordinate of bottom left
                w = int(_w)        # width
                h = int(_h)        # height

                # Loop over all trackers and check if the center point of the face is within
                # the tracker box, AND the center of the tracker box is within the region of
                # the detected face.
                
                xcenter = x + 0.5 * w
                ycenter = y + 0.5 * h

                matchedFaceID = None

                for faceID in faceTrackers.keys():

                    tracked_position = faceTrackers[faceID].get_position()

                    tracker_x = int(tracked_position.left())      # x-coordinate of bottom left
                    tracker_y = int(tracked_position.top())       # y-coordinate of bottom left
                    tracker_w = int(tracked_position.width())     # width
                    tracker_h = int(tracked_position.height())    # height

                    tracker_xcenter = tracker_x + 0.5 * tracker_w
                    tracker_ycenter = tracker_y + 0.5 * tracker_h

                    if (    (xcenter >= tracker_x) and (xcenter <= (tracker_x + tracker_w)) 
                        and (ycenter >= tracker_y) and (ycenter <= (tracker_y + tracker_h)) 
                        and (tracker_xcenter >= x) and (tracker_xcenter <= (x + w)) 
                        and (tracker_ycenter >= y) and (tracker_ycenter <= (y + h))):

                        matchedFaceID = faceID


                # If no matched faceID, create a new tracker

                if matchedFaceID is None:

                    tracker = dlib.correlation_tracker()
                    tracker.start_track(baseImage, dlib.rectangle(x-10, y-20, x+w+10, y+h+20))
                    faceTrackers[currentFaceID] = tracker
                    currentFaceID += 1


        # Loop over all the trackers and draw the rectangle around the detected faces.
        
        for faceID in faceTrackers.keys():
            
            tracked_position = faceTrackers[faceID].get_position()

            tracker_x = int(tracked_position.left())
            tracker_y = int(tracked_position.top())
            tracker_w = int(tracked_position.width())
            tracker_h = int(tracked_position.height())

            myColor = (0,165,255)    # light blue

            cv2.rectangle(resultImage, (tracker_x, tracker_y), (tracker_x + tracker_w , tracker_y + tracker_h), myColor, 2)


        # Resize the result image back to a larger version, and show it on the screen
        
        largeImage = cv2.resize(resultImage, (775,600))
        cv2.imshow("base-image", baseImage)
        cv2.imshow("result-image", largeImage)
        cv2.waitKey(1)


# Allow the user to interrupt with Ctrl-c

except KeyboardInterrupt as e:
    pass

    
video.release()
cv2.destroyAllWindows()


